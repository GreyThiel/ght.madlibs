//Mad Libs
//Grey Thiel

//includes
#include<iostream>
#include<fstream>
#include<conio.h>
#include<string>

using namespace std;

//function prototypes


int main()
{
	//arrays
	int const INPUTS = 12;
	string arrInputs[INPUTS] = {"n adjective", " sport", " city", " person", "n action verb (past tense)", " vehicle", " place", " noun",
	"n adjective", " food", " liquid", "n adjective"};

	//collect data
	for (int i = 0; i < INPUTS; i++)
	{
		cout << "Enter a" << arrInputs[i] << ": ";
		getline(cin, arrInputs[i]);
	}

	//enter userdata into madlib and print to console
	string text;

	text = "One day my " + arrInputs[0] + " friend and I decided to go to a " + arrInputs[1] +
		" game in " + arrInputs[2] + ".\n" + "We really wanted to see " + arrInputs[3] + " play.\n" +
		"So we " + arrInputs[4] + " in " + arrInputs[5] + " and headed down to the " + arrInputs[6] +
		" and bought some " + arrInputs[7] + ".\n" + "We watched the game and it was " + arrInputs[8] +
		".\n" + "We ate some " + arrInputs[9] + " and drank some " + arrInputs[10] + ".\n" + "We had "
		+ "a " + arrInputs[11] + " time, and can't wait to go again.\n";
	
	cout << text << "\n";

	//ask to save
	cout << "Would you like to save? ";
	char decision;
	cin >> decision;
	
	//save to file
	if (decision == 'y' || decision == 'Y')
	{
		ofstream ofs("text.txt");
		
		ofs << text;

		ofs.close();

		cout << "File saved to text.txt";
	}

	(void)_getch();
	return 0;
}